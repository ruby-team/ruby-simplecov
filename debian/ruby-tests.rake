require 'gem2deb/rake/spectask'

Gem2Deb::Rake::RSpecTask.new do |spec|
  spec.pattern = './spec/**/*_spec.rb'
  spec.exclude_pattern = [
    './spec/default_formatter_spec.rb', # needs simplecov_json_formatter
    './spec/gemspec_spec.rb',           # assumes bundler is loaded
  ].join(",")
end
